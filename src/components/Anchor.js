import React from 'react';

class Anchor extends React.Component {
  constructor(props) {
    super(props);
    this.state = { display: false };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();

    //re-render pate
    this.setState({ display: !this.state.display });

    //not re-render page
    //this.state.display = !this.state.display;

    console.log('state => ' + this.state.display);
  }

  render() {
    return (
      <div>
        <a href="#" onClick={this.handleClick}>
          wnaat to by new car ?
        </a>
        {this.state.display ? <p>Call +261 33 23 177 23 now!</p> : null}
      </div>
    );
  }
}

export default Anchor;
