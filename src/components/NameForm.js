import React from 'react';
import ReactDOM from 'react-dom';

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '', expression: '147.45*23.12' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.calcul = this.calcul.bind(this);
    this.arrayTest = this.arrayTest.bind(this);
  }

  componentDidMount() {
    //this.arrayTest();
    console.log('componentDidMount => called');
  }

  shouldComponentUpdate(nextProps: Object, nextState: Object) {
    console.log('shouldComponentUpdate => called');
    return true;
  }

  componentDidUpdate() {
    console.log('componentDidUpdate => called');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount => called');
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
    console.log('event => ' + event.target.value);
  }

  handleSubmit(event) {
    //alert('Le nom a été soumis : ' + this.state.value);
    this.calcul();
    console.log('value => ' + this.state.value);
    event.preventDefault();
    
  }

  arrayTest() {
    /*var personnel = [
      {
        id: 5,
        name: "Luke Skywalker",
        pilotingScore: 98,
        shootingScore: 56,
        isForceUser: true,
      },
      {
        id: 82,
        name: "Sabine Wren",
        pilotingScore: 73,
        shootingScore: 99,
        isForceUser: false,
      },
      {
        id: 22,
        name: "Zeb Orellios",
        pilotingScore: 20,
        shootingScore: 59,
        isForceUser: false,
      },
      {
        id: 15,
        name: "Ezra Bridger",
        pilotingScore: 43,
        shootingScore: 67,
        isForceUser: true,
      },
      {
        id: 11,
        name: "Caleb Dume",
        pilotingScore: 71,
        shootingScore: 85,
        isForceUser: true,
      },
    ];

    const totalJediScore = personnel
      .filter(person => person.isForceUser)
      .map(jedi => jedi.pilotingScore + jedi.shootingScore)
      .reduce((acc, score) => acc + score, 0);

    console.log('JEDI=>' + totalJediScore);*/

    var array=[1,2,3,4,5];
    console.log('array1 =>' + array.splice(3,2));
    console.log('array2 =>' + array);
    //console.log('array1 =>' + array.slice(3,5));
    //console.log('array2 =>' + array);
  }

  calcul() {
    let pattern = /\d*(\+|\*|\-|\/)\d*(\.?)\d*/;
    //let pattern = /\d*(\.?)\d*(\+|\*|\-|\/)\d*(\.?)\d*/;
    //let m = this.state.expression.match(pattern)
    //console.log('m =>' + typeof(m));
    let matches = pattern.exec(this.state.expression)
    console.log('match lolo=>' + matches);
    let chif1 = this.state.expression.split(matches[2])[0];
    let chif2 = this.state.expression.split(matches[2])[1];
    console.log('a=>' + chif1);
    console.log('b=>' + chif2);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Nom :
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Envoyer" />
      </form>
    );
  }
}

export default NameForm;
