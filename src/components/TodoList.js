import React from 'react';
import ReactDOM from 'react-dom';

class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
  }

  handleItemClick = (item, event) => {
    /*event.nativeEvent.stopImmediatePropagation();
    event.stopPropagation();
    event.preventDefault();*/
    /*if (item.done) {
      return;
    }*/
    console.log('click here => ' + JSON.stringify(item));
  };

  render() {
    const items = [
      { text: 'test1', done: true },
      { text: 'test2', done: false },
      { text: 'test3', done: false }
    ];
    return (
      <ul>
        {items.map((item, index) => (
          <li onClick={() => this.handleItemClick(item)} key={index}>
            {item.text}
          </li>
        ))}
      </ul>
    );
  }
}

export default TodoList;
