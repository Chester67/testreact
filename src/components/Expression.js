import React from 'react';

class Expression extends React.Component {
  constructor(props) {
    super(props);
    this.state = { display: false };
    //expression new in react >= 16.2 with empty tag and no need to write root node
  }

  arrow = (param) => {
    return 1 + param;
  };

  render() {
    return (
      <>
        <td>hello {this.arrow(2)}</td>
        <td>world</td>
      </>
    );
  }
}

export default Expression;
