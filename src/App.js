import React from 'react';
import logo from './logo.svg';
import NameForm from './components/NameForm';
import TodoList from './components/TodoList';
import Anchor from './components/Anchor';
import Expression from './components/Expression';
import Empty from './components/Empty'
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <NameForm />
      </header>
    </div>
  );
}

export default App;
